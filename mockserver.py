#!/usr/bin/env python

"""
An echo server for test.
"""

from __future__ import print_function
from SocketServer import TCPServer, ThreadingMixIn, BaseRequestHandler
import os
import sys
import time
import socket


def current_time():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))


class MockTCPServer(TCPServer):
    allow_reuse_address = True

    def get_request(self):
        while True:
            if os.path.exists('/tmp/__mockaccept'):
                return self.socket.accept()
            else:
                time.sleep(.001)


class ThreadingTCPServer(ThreadingMixIn, MockTCPServer):

    def server_bind(self):
        MockTCPServer.server_bind(self)


class EchoHandler(BaseRequestHandler):
    # A timeout to apply to the request socket, if not None.
    timeout = None

    # Disable nagle algorithm for this socket, if True.
    # Use only when wbufsize != 0, to avoid small packets.
    disable_nagle_algorithm = False

    def setup(self):
        self.connection = self.request
        if self.timeout is not None:
            self.connection.settimeout(self.timeout)
        if self.disable_nagle_algorithm:
            self.connection.setsockopt(socket.IPPROTO_TCP,
                                       socket.TCP_NODELAY, True)

    def log(self, data):
        sys.stdout.write(data)
        sys.stdout.flush()

    def handle(self):
        self.peer = "%s:%d" % (self.client_address[0], self.client_address[1])
        print("\nAccepted: %s\n" % self.peer)
        self.connection.settimeout(2)

        data = "DUMMY"
        while data != "":
            try:
                data = self.connection.recv(1)
                # self.connection.close()
                # time.sleep(1)
                self.log(data)
            except socket.error:
                data = ""
                self.connection.close()
                self.log("Closed: %s\n" % self.peer)

if __name__ == '__main__':
    ThreadingTCPServer.allow_reuse_address = True
    ThreadingTCPServer.address_family = socket.AF_INET
    server = ThreadingTCPServer(("", 29999), EchoHandler)
    server.serve_forever()
