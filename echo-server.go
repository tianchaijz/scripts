// $ 6g echo.go && 6l -o echo echo.6
// $ ./echo
//
//  ~ in another terminal ~
//
// $ nc localhost 3540

package main

import (
	"bufio"
	"flag"
	"fmt"
	"net"
	"time"
)

func main() {
	address := flag.String("address", "127.0.0.1", "listen address")
	port := flag.String("port", "5565", "listen port")
	flag.Parse()

	addr := *address + ":" + *port
	fmt.Printf("Listening on: %s\n", addr)

	server, err := net.Listen("tcp", addr)
	if err != nil {
		panic("couldn't start listening: " + err.Error())
	}

	conns := clientConns(server)
	for {
		go handleConn(<-conns)
	}
}

func clientConns(listener net.Listener) chan net.Conn {
	ch := make(chan net.Conn)
	i := 0
	go func() {
		for {
			client, err := listener.Accept()
			if client == nil {
				fmt.Printf("couldn't accept: " + err.Error())
				continue
			}
			i++
			fmt.Printf("%d: %v <-> %v\n", i, client.LocalAddr(), client.RemoteAddr())
			ch <- client
		}
	}()
	return ch
}

func handleConn(client net.Conn) {
	raddr := client.RemoteAddr().String()
	b := bufio.NewReader(client)
	for {
		client.SetReadDeadline(time.Now().Add(1 * time.Second))
		buf := make([]byte, 1024)
		_, err := b.Read(buf)
		if err != nil {
			if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
				// keep the connection open, we are just checking to see if
				// we are shutting down

			} else {
				fmt.Println()
				fmt.Printf("Close connection: %s, <%v>\n", raddr, err)
				break
			}
			continue
		}
		// client.Write(buf)
		fmt.Print(string(buf))
	}
}
