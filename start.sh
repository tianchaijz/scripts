#!/bin/sh

service=$1

if [ "$service" == "surume" ]; then
    python paramiko_forwarding.py -p 2963 -u root -r 192.168.1.220:963 124.160.114.202:65422
elif [ "$service" == "smoker" ]; then
    python paramiko_forwarding.py --local-addr 127.0.0.1 -p 5000 -u vagrant -r 127.0.0.1:5000 127.0.0.1:2222
elif [ "$service" == "redis43" ]; then
    python paramiko_forwarding.py -p 1091 -u root -r 192.168.1.43:1091 124.160.114.202:65422
elif [ "$service" == "elasticsearch" ]; then
    python paramiko_forwarding.py -p 9200 -u root -r 192.168.21.11:9200 124.160.114.202:65422
elif [ "$service" == "lnaes" ]; then
    python paramiko_forwarding.py -p 9201 -u root -r 192.168.60.17:9200 60.191.72.6:65422
fi
